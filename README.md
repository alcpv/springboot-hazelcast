# Springboot Hazelcast Demo Project

## How to run it
 * start the local mysql db and nginx as load balancer (make sure to comment out the app1 and app2 service in docker-compose.yml) the start the apps <br>
  `docker-compose up -d --build` <br>
  `docker-compose logs -f`
  `mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Dserver.port=8081"`
  `mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Dserver.port=8082"`
 * build the app: <br>
  `mvn clean install`
 * start the apps: with docker containers and local mysql db and nginx as load balancer: (app1 and app2 services are present in docker-compose.yml) <br>
  `mvn clean install` <br>
  `docker-compose up -d --build` <br>
  `docker-compose logs -f`  
 * stop the docker stack cleanly:
  `docker-compose down -v --remove-orphans`
 * `docker-compose rm -v`
