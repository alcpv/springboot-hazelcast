package com.hazelcast.springboot.caching.service;

import com.hazelcast.cluster.Cluster;
import com.hazelcast.core.HazelcastInstance;

import com.hazelcast.map.IMap;
import com.hazelcast.map.LocalMapStats;
import com.hazelcast.springboot.caching.dto.CacheConstants;
import com.hazelcast.springboot.caching.model.Book;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
@Service
public class CacheInfoService {

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Autowired
    private CacheManager cacheManager;

    @Autowired
    private BookService bookService;

    public Cache getCacheMap(String cacheMapName) {
        return cacheManager.getCache(cacheMapName);
    }

    private String convertTime(long time){
        Date date = new Date(time);
        Format format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(date);
    }

    public Map<String, Object> getCacheMapStatistics(String cacheMap) {
        Map<String, Object> cacheMapStatistics = new HashMap<>();
        IMap<String, String> bookCacheMap = hazelcastInstance.getMap(cacheMap);
        LocalMapStats mapStatistics = bookCacheMap.getLocalMapStats();

        cacheMapStatistics.put("OwnedEntryMemoryCost", mapStatistics.getOwnedEntryMemoryCost());
        cacheMapStatistics.put("CreationTime", convertTime(mapStatistics.getCreationTime()));
        cacheMapStatistics.put("HeapCost", mapStatistics.getHeapCost());
        cacheMapStatistics.put("OperationCount", mapStatistics.getGetOperationCount());
        cacheMapStatistics.put("LastUpdateTime", convertTime(mapStatistics.getLastUpdateTime()));
        cacheMapStatistics.put("LastAccessTime", convertTime(mapStatistics.getLastAccessTime()));
        cacheMapStatistics.put("Hits", mapStatistics.getHits());
        cacheMapStatistics.put("TotalGetLatency", mapStatistics.getTotalGetLatency());
        cacheMapStatistics.put("TotalPutLatency", mapStatistics.getTotalPutLatency());
        cacheMapStatistics.put("TotalRemoveLatency", mapStatistics.getTotalRemoveLatency());
        cacheMapStatistics.put("TotalSetLatency", mapStatistics.getTotalSetLatency());
        cacheMapStatistics.put("ServiceName", bookCacheMap.getServiceName());
        cacheMapStatistics.put("PartitionKey", bookCacheMap.getPartitionKey());
        return cacheMapStatistics;
    }

    public Map<String, Object> getCacheClusterDetails() {
        Map<String, Object> cacheClusterDetails = new HashMap<>();
        Cluster cluster = hazelcastInstance.getCluster();
        cacheClusterDetails.put("ClusterState", cluster.getClusterState());
        cacheClusterDetails.put("Members", cluster.getMembers());
        cacheClusterDetails.put("HotRestartService", cluster.getHotRestartService());
        return cacheClusterDetails;
    }

    public Cache initBookCacheMapByIsbn() {
        log.info("Initialising {}", CacheConstants.BOOKS_CACHE_ISBN);
        List<Book> books = bookService.getAll();
        books.stream().forEach(
                b -> cacheManager.getCache(CacheConstants.BOOKS_CACHE_ISBN).put(b.getIsbn(), b)
        );
        log.info("Init cache {} done", CacheConstants.BOOKS_CACHE_ISBN);
        return getCacheMap(CacheConstants.BOOKS_CACHE_ISBN);
    }

    public Cache initBookCacheMapByAuthorAndName() {
        log.info("Initialising {}", CacheConstants.BOOKS_CACHE_BY_AUTHOR_AND_NAME);
        List<Book> books = bookService.getAll();
        books.stream().forEach(
                b -> cacheManager.getCache(CacheConstants.BOOKS_CACHE_BY_AUTHOR_AND_NAME).put(b.getAuthor() + "-" + b.getName(), b)
        );
        log.info("Init cache {} done", CacheConstants.BOOKS_CACHE_BY_AUTHOR_AND_NAME);
        return getCacheMap(CacheConstants.BOOKS_CACHE_BY_AUTHOR_AND_NAME);
    }

}
