package com.hazelcast.springboot.caching.service;

import com.hazelcast.springboot.caching.dto.CacheConstants;
import com.hazelcast.springboot.caching.model.Book;
import com.hazelcast.springboot.caching.persist.BookRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Log4j2
@CacheConfig(cacheNames = {CacheConstants.BOOKS_CACHE_ISBN, CacheConstants.BOOKS_CACHE_BY_AUTHOR_AND_NAME})
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public List<Book> getAll() {
        log.info("Retrieving all books");
        return bookRepository.findAll();
    }

    public Book getById(Long id) {
        log.info("Retrieving book with id: {}", id);
        return bookRepository.findById(id).orElseThrow(() -> new RuntimeException("Book with id: " + id + " not " +
                "found"));
    }

    @Cacheable(value = CacheConstants.BOOKS_CACHE_ISBN, key = "#isbn", unless = "#result == null")
    public Book getByIsbn(String isbn) {
        log.info("Retrieving book with isbn: {}", isbn);
        return bookRepository.findByIsbn(isbn).orElseThrow(() -> new RuntimeException("Book with isbn: " + isbn +
                "was not found"));
    }

    public Book getByIsbnNonCached(String isbn) {
        log.info("Retrieving book with isbn: {}", isbn);
        return bookRepository.findByIsbn(isbn).orElseThrow(() -> new RuntimeException("Book with isbn: " + isbn +
                "was not found"));
    }

    @CachePut(value = CacheConstants.BOOKS_CACHE_ISBN, key = "#book.isbn", unless = "#result == null")
    public Book create(Book book) {
        log.info("Saving a new book with isbn: {}", book.getIsbn());
        return bookRepository.save(book);
    }

    @CachePut(value = CacheConstants.BOOKS_CACHE_ISBN, key = "#book.isbn", unless = "#result == null")
    public Book updateByIsbn(String isbn, Book book) {
        if (!isbn.equals(book.getIsbn())) {
            log.error("update not possible due to different isbn");
            throw new RuntimeException("Book cannot be updated");
        }
        log.info("Updating a book with isbn: {}", book.getIsbn());
        Optional<Book> optionalBook = bookRepository.findByIsbn(book.getIsbn());
        if (optionalBook.isPresent()) {
            Book existingBook = optionalBook.get();
            existingBook.setIsbn(book.getIsbn());
            existingBook.setAuthor(book.getAuthor());
            existingBook.setName(book.getName());
            return bookRepository.save(existingBook);
        } else {
            log.error("Book to update with isbn {} not found", book.getIsbn());
            throw new RuntimeException("Book does not exists");
        }
    }

    @CacheEvict(value = CacheConstants.BOOKS_CACHE_ISBN, key = "#isbn")
    public void deleteByIsbn(String isbn) {
        log.info("Deleting book with isbn: {}", isbn);
        Optional<Book> book = bookRepository.findByIsbn(isbn);
        if (book.isPresent()) {
            bookRepository.delete(book.get());
        } else {
            log.error("Book with isbn {} not found!", isbn);
        }
    }

    @Cacheable(value = CacheConstants.BOOKS_CACHE_BY_AUTHOR_AND_NAME, key = "#author+'-'+#name",
               unless = "#result == null")
    public Book getByAuthorAndName(String author, String name) {
        log.info("Retrieving book with author: {} and name: {}", author, name);
        return bookRepository.findByAuthorAndName(author.trim(), name.trim()).orElseThrow(
                () -> new RuntimeException("Book with author: " + author + " and name: " + name + " not found"));
    }

    @CacheEvict(value = CacheConstants.BOOKS_CACHE_BY_AUTHOR_AND_NAME, key = "#author+'-'+#name")
    public void deleteByAuthorAndName(String author, String name) {
        log.info("Deleting book with author: {} and name: {}", author, name);
        Book book = bookRepository.findByAuthorAndName(author.trim(), name.trim()).orElseThrow(
                () -> new RuntimeException("Book with author: " + author + " and name: " + name + " not found"));
        bookRepository.delete(book);
    }

}
