package com.hazelcast.springboot.caching.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.cache.CacheManager;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class InitCache {

    @Autowired
    private CacheInfoService cacheInfoService;
    @Autowired
    private CacheManager cacheManager;

    @EventListener(classes = ApplicationStartedEvent.class)
    public void listenToStart(ApplicationStartedEvent event) {
        log.info("Initialising cache ... triggered on {}", event.getClass().getName());
        cacheInfoService.initBookCacheMapByIsbn();
        cacheInfoService.initBookCacheMapByAuthorAndName();
        log.info("Caches in use:{}", cacheManager.getCacheNames().toString());

    }
}
