package com.hazelcast.springboot.caching.controller;

import com.hazelcast.springboot.caching.model.Book;
import com.hazelcast.springboot.caching.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping(path = "/id/{id}")
    public Book getBookById(@PathVariable("id") Long id) {
        return bookService.getById(id);
    }

    @GetMapping(path = "")
    public List<Book> getAllBooks() {
        return bookService.getAll();
    }

    @GetMapping(path = "/isbn/non-cached/{isbn}")
    public Book getBookByIsbnNonCached(@PathVariable("isbn") String isbn) {
        return bookService.getByIsbnNonCached(isbn);
    }

    @GetMapping(path = "/isbn/{isbn}")
    public Book getBookByIsbn(@PathVariable("isbn") String isbn) {
        return bookService.getByIsbn(isbn);
    }

    @PostMapping(path = "/isbn")
    public Book create(@RequestBody Book book) {
        return bookService.create(book);
    }

    @PutMapping(path = "/isbn/{isbn}")
    public Book updateBook(@PathVariable(name = "isbn") String isbn, @RequestBody Book book) {
        return bookService.updateByIsbn(isbn, book);
    }

    @DeleteMapping(path = "/isbn/{isbn}")
    public void deleteBook(@PathVariable(name = "isbn") String isbn) {
        bookService.deleteByIsbn(isbn);
    }

    @GetMapping(path = "/fields")
    public Book getByAuthorAndName(@RequestParam("author") String author, @RequestParam("name") String name) {
        return bookService.getByAuthorAndName(author, name);
    }

    @DeleteMapping(path = "/fields")
    public void deleteByAuthorAndName(@RequestParam("author") String author, @RequestParam("name") String name) {
        bookService.deleteByAuthorAndName(author, name);
    }

}

