package com.hazelcast.springboot.caching.controller;

import com.hazelcast.springboot.caching.dto.CacheConstants;
import com.hazelcast.springboot.caching.service.CacheInfoService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Log4j2
@RestController
@RequestMapping("/cache")
public class CacheInfoController {

    @Autowired
    private CacheInfoService cacheInfoService;

    @GetMapping(path = "/map/{cacheMapName}")
    public Cache getCacheMap(@PathVariable(value = "cacheMapName") String cacheMapName) {
        log.info("Fetching cache map: {}", cacheMapName);
        return cacheInfoService.getCacheMap(cacheMapName);
    }

    @GetMapping(path = "/map/init/{cacheMapName}")
    public Cache initCacheMap(@PathVariable(value = "cacheMapName") String cacheMapName) {
        log.info("Initialising cache map: {}", cacheMapName);
        if (cacheMapName.equals(CacheConstants.BOOKS_CACHE_ISBN)) {
            return cacheInfoService.initBookCacheMapByIsbn();
        } else if (cacheMapName.equals(CacheConstants.BOOKS_CACHE_BY_AUTHOR_AND_NAME)) {
            return cacheInfoService.initBookCacheMapByAuthorAndName();
        }
        else {
            return null;
        }
    }

    @DeleteMapping(path = "/map/{cacheMapName}")
    public void clearCacheMap(@PathVariable(value = "cacheMapName") String cacheMapName) {
        log.info("Clearing cache map: {}", cacheMapName);
        cacheInfoService.getCacheMap(cacheMapName).clear();
    }

    @GetMapping(path = "/map-statistics/{cacheMapName}")
    public Map<String, Object> getCacheMapStatistics(@PathVariable(value = "cacheMapName") String cacheMapName) {
        return cacheInfoService.getCacheMapStatistics(cacheMapName);
    }

    @GetMapping(path = "/cluster-details")
    public Map<String, Object> getCacheClusterDetails() {
        return cacheInfoService.getCacheClusterDetails();
    }
}
