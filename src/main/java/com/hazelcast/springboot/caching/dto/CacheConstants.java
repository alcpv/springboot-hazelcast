package com.hazelcast.springboot.caching.dto;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CacheConstants {
    public static final String BOOKS_CACHE_ISBN = "books-cache-isbn";
    public static final String BOOKS_CACHE_BY_AUTHOR_AND_NAME = "books-cache-author-and-name";
}
