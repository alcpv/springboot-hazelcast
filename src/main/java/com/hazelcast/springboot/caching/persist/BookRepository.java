package com.hazelcast.springboot.caching.persist;

import com.hazelcast.springboot.caching.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    Optional<Book> findByIsbn(String isbn);
    Optional<Book> findById(Long id);
    Optional<Book> findByAuthorAndName(String author, String name);
    List<Book> findAll();
}
